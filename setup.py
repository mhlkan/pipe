from setuptools import setup
setup(
    name='pipe',
    version='0.1',
    description='Basic Pip Snippets.',
    license='MIT',
    author='mhlkan',
    py_modules=['pipe'],
    url='https://github.com/mhlkan/pipe',
    entry_points={
        'console_scripts': [
            'pipe = pipe:main'
        ]
    }
)
