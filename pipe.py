import subprocess as sp
import sys
args = sys.argv[1:]
class pipe:
    def __init__(self, pkg=""):
        self.pkg = pkg
    def install(self):
        sp.call(['pip', 'install', self.pkg])
    def uninstall(self):
        sp.call(['pip', 'uninstall', self.pkg])
    def upgrade(self):
        sp.call(['pip', 'install', '--upgrade', self.pkg])
    def list(self):
        sp.call(['pip', 'list'])
def main():
    try:
        args[0]
    except:
        print("For help; type `pipe h`")
        return
    if args[0] == 'i':
        pipe(args[1]).install()
    elif args[0] == 'r':
        pipe(args[1]).uninstall()
    elif args[0] == 'l':
        pipe().list()
    elif args[0] == 'u':
        pipe(args[1]).upgrade()
    elif args[0] == 'h':
        print("""
pipe i ... : Installs package
pipe r ... : Uninstalls package
pipe u ... : Upgrades package
pipe l : Lists all packages
pipe h : Shows you this
""")
if __name__ == '__main__':
    main()
